package Playlist.Dependencies;

import Playlist.Application.UseCase;
import Playlist.Dependencies.Struct.ScheduleSimple;
import Playlist.Domain.Schedule;
import Playlist.Dependencies.Interface.Storage;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static Playlist.Dependencies.DateConvert.formatDateToStringDDMMYYYY;
import static Playlist.Dependencies.Struct.ScheduleSimple.scheduleToScheduleSimple;

public class ViewSchedule
{
    private final Storage storage;
    private Instant date;

    public ViewSchedule(Storage storage, Instant date)
    {
        this.storage = storage;
        this.date = date;
    }

    public List<ScheduleSimple> viewSchedule()
    {
        List<Schedule> schedules = storage.getSchedules(DateConvert.formatDateToStringDDMMYYYY(date.atZone(UseCase.zone)));
        return processingSchedule(schedules);
    }

    private List<ScheduleSimple> processingSchedule(List<Schedule> schedules)
    {
        List<ScheduleSimple> scheduleSimples = new ArrayList<ScheduleSimple>(schedules.size());
        for (int i=0; i<schedules.size(); i++)
            scheduleSimples.add(scheduleToScheduleSimple(schedules.get(i)));
        return scheduleSimples;
    }
}