package Playlist.Dependencies.Struct;

import Playlist.Application.UseCase;
import Playlist.Dependencies.DateConvert;
import Playlist.Domain.Schedule;

public class ScheduleSimple
{
    private String type;
    private String startTime;
    private String endTime;

    private ScheduleSimple(String type, String startTime, String endTime)
    {
        this.type = type;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getType()
    {
        return type;
    }

    public String getStartTime()
    {
        return startTime;
    }

    public String getEndTime()
    {
        return endTime;
    }

    public static ScheduleSimple scheduleToScheduleSimple(Schedule schedule)
    {
        String type = schedule.getType().toString();
        String startTime = DateConvert.formatDateToStringHHmm(schedule.getStartDate().atZone(UseCase.zone));
        String endTime = DateConvert.formatDateToStringHHmm(schedule.getEndDate().atZone(UseCase.zone));
        return new ScheduleSimple(type, startTime ,endTime);
    }
}
