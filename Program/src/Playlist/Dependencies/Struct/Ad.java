package Playlist.Dependencies.Struct;

import Playlist.Domain.Schedule;

import java.time.Instant;

public class Ad
{
    private int id;
    private int frequency;
    private long duration;
    private String hash;

    public Ad(int id, int frequency, long duration)
    {
        this.id = id;
        this.frequency = frequency;
        this.duration = duration;
    }

    public Ad(int id, long duration, String hash)
    {
        this.id = id;
        this.duration = duration;
        this.hash = hash;
    }

    public int getId()
    {
        return this.id;
    }

    public long getDuration()
    {
        return this.duration;
    }

    public int getFrequency()
    {
        return this.frequency;
    }

    public String getHash()
    {
        return this.hash;
    }

    public Schedule adToSchedule(int id, Instant startDate)
    {
        return hash==null ? Schedule.ad(id, this.id, startDate, this.duration):Schedule.ad(id, this.id, startDate, this.duration, hash);
    }
}