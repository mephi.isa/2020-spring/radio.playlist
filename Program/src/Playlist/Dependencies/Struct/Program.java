package Playlist.Dependencies.Struct;

import Playlist.Domain.Schedule;
import java.time.Instant;

public class Program
{
    private int id;
    private long duration;
    private Instant startDate;

    public Program(int id, long duration, Instant startDate)
    {
        this.id = id;
        this.duration = duration;
        this.startDate = startDate;
    }

    public int getId()
    {
        return this.id;
    }

    public long getDuration()
    {
        return this.duration;
    }

    public Instant getStartDate()
    {
        return startDate;
    }

    public Schedule programToSchedule(int id)
    {
        return Schedule.program(id, this.id, this.startDate, this.duration);
    }
}
