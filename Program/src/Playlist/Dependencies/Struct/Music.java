package Playlist.Dependencies.Struct;

import Playlist.Domain.Schedule;
import java.time.Instant;

public class Music
{
    private int id;
    private long duration;
    private String hash;
    private String nameMusic;
    private String nameArtist;

    public Music(int id, long duration, String hash, String nameMusic, String nameArtist)
    {
        this(id, duration, hash);
        this.nameMusic = nameMusic;
        this.nameArtist = nameArtist;
    }

    public Music(int id, long duration, String hash)
    {
        this.id = id;
        this.duration = duration;
        this.hash = hash;
    }

    public int getId()
    {
        return this.id;
    }

    public long getDuration()
    {
        return this.duration;
    }

    public String getHash()
    {
        return this.hash;
    }

    public String getNameMusic()
    {
        return this.nameMusic;
    }

    public String getNameArtist()
    {
        return this.nameArtist;
    }

    public Schedule musicToSchedule(int id, Instant startDate)
    {
        return Schedule.music(id, this.id, startDate, this.duration, this.hash);
    }
}
