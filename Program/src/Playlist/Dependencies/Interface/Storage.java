package Playlist.Dependencies.Interface;

import Playlist.Domain.Schedule;

import java.time.Instant;
import java.util.List;

public interface Storage
{
    void setListSchedule(List<Schedule> schedules);

    void setSchedule(Schedule schedule);

    void updateHashStorage(String id, String hash);

    List<Schedule> getSchedules(String date);

    String getHash(String id);

    int getId(String date);
}
