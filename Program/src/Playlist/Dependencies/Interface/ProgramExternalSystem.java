package Playlist.Dependencies.Interface;

import Playlist.Dependencies.Struct.Program;

import java.util.List;

public interface ProgramExternalSystem
{
    List<Program> getPrograms(String date);
}