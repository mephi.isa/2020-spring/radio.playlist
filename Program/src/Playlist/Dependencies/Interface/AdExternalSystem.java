package Playlist.Dependencies.Interface;

import Playlist.Dependencies.Struct.Ad;

import java.util.List;

public interface AdExternalSystem
{
    List<Ad> getAds(String date);

    Ad getAd();

    String getHash(int id);
}
