package Playlist.Dependencies.Interface;

import Playlist.Dependencies.Struct.Music;

import java.util.List;

public interface MusicExternalSystem
{
    List<Music> getMusics(int durationFreeTime);

    List<Music> getMusics();
}
