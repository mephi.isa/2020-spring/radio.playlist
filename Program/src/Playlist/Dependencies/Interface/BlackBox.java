package Playlist.Dependencies.Interface;

public interface BlackBox
{
    void playFile(String hash);

    void startOnline();

    //Возвращает хеш
    String endOnline();
}
