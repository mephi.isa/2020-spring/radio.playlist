package Playlist.Dependencies;

import Playlist.Application.UseCase;
import Playlist.Dependencies.Interface.AdExternalSystem;
import Playlist.Dependencies.Interface.MusicExternalSystem;
import Playlist.Dependencies.Interface.ProgramExternalSystem;
import Playlist.Dependencies.Struct.Ad;
import Playlist.Dependencies.Struct.Music;
import Playlist.Dependencies.Struct.Program;
import Playlist.Domain.Schedule;
import Playlist.Dependencies.Interface.Storage;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static Playlist.Dependencies.DateConvert.formatDateToStringDDMMYYYY;

public class GenerateSchedule
{
    private Instant date;
    private List<Schedule> schedules;
    private static final int minuteInDay = 60*24;
    private Storage storage;
    private AdExternalSystem adExternalSystem;
    private MusicExternalSystem musicExternalSystem;
    private ProgramExternalSystem programExternalSystem;
    private boolean[] busyTime = new boolean[minuteInDay];

    private class FreeTime
    {
        public int startFreeTime;
        public int durationFreeTime;

        public FreeTime(int startFreeTime, int durationFreeTime)
        {
            this.startFreeTime = startFreeTime;
            this.durationFreeTime = durationFreeTime;
        }
    }

    public GenerateSchedule(Storage storage, AdExternalSystem adExternalSystem, MusicExternalSystem musicExternalSystem, ProgramExternalSystem programExternalSystem, Instant date)
    {
        this.date = date;
        schedules = new ArrayList<Schedule>();
        this.storage = storage;
        this.adExternalSystem = adExternalSystem;
        this.musicExternalSystem = musicExternalSystem;
        this.programExternalSystem = programExternalSystem;
    }

    public void generateSchedule()
    {
        processingArrayProgram(createProgram());
        List<Ad> ads = createAd();
        processingArrayAd(ads, countAdWithFrequency(ads));
        generateMusic();
    }

    //Находит свободный интервал и отправляет запрос в стороннюю систему, после этого добавляет все музыкальные композиции, которые пришли
    private void generateMusic()
    {
        while (true)
        {
            FreeTime freeTime = solvedStartTime();
            if (freeTime==null)
                break;
            List<Music> musics = requestMusic(freeTime.durationFreeTime);
            if (musics!=null)
                processingArrayMusic(musics, DateConvert.formatIntToDate(date, freeTime.startFreeTime));
        }

    }

    //требуется доработка
    private List<Music> requestMusic(int durationFreeTime)
    {
        return musicExternalSystem.getMusics(durationFreeTime);
    }

    private List<Program> createProgram()
    {
        return programExternalSystem.getPrograms(formatDateToStringDDMMYYYY(date.atZone(UseCase.zone)));
    }

    private List<Ad> createAd()
    {
        return adExternalSystem.getAds(formatDateToStringDDMMYYYY(date.atZone(UseCase.zone)));
    }

    //Логика добавления рекламного ролика в расписание
    private void processingArrayAd(List<Ad> ads, int countAdWithFrequency)
    {
        if (countAdWithFrequency==0)
            return;
        int step = minuteInDay/countAdWithFrequency;
        int j=0;
        for (int i=0; i<ads.size();i++)
            j+=processingAd(ads.get(i), j*step, step);
    }

    //возвращает обработаннное количество рекламных роликов
    private int processingAd(Ad ad, int iFirst, int step)
    {
        int i=0;
        while (i<ad.getFrequency())
        {
            Schedule schedule = ad.adToSchedule(0, DateConvert.formatIntToDate(date, iFirst+step*i));
            schedules.add(schedule);
            setBusyTime(schedule.getStartDate(), schedule.getEndDate());
            i++;
        }
        return i;
    }

    //Возвращает общее количество рекламных роликов
    private int countAdWithFrequency(List<Ad> ads)
    {
        int count = 0;
        for (int i=0; i<ads.size(); i++)
            count += ads.get(i).getFrequency();
        return count;
    }

    private void processingArrayProgram(List<Program> programs)
    {
        for (int i=0; i<programs.size(); i++)
            processingProgram(programs.get(i));
    }

    private void processingProgram(Program program)
    {
        Schedule schedule = program.programToSchedule(0);
        schedules.add(schedule);
        setBusyTime(schedule.getStartDate(), schedule.getEndDate());
    }

    private void processingArrayMusic(List<Music> musics, Instant startTime)
    {
        for (int i=0; i<musics.size(); i++)
            startTime = processingMusic(musics.get(i), startTime);
    }

    //Возвращает дату окончания
    private Instant processingMusic(Music music, Instant startDate)
    {
        Schedule schedule = music.musicToSchedule(0, startDate);
        schedules.add(schedule);
        return schedule.getEndDate();
    }

    //Вычисление старта элемента расписания и свободный промежуток
    private FreeTime solvedStartTime()
    {
        int startFreeTime=0;
        int durationFreeTime=0;
        boolean hasFreeTime = false;
        for (int i=0; i<minuteInDay; i++)
        {
            if(!busyTime[i])
            {
                busyTime[i] = true;
                startFreeTime=!(hasFreeTime)?i:startFreeTime;
                durationFreeTime++;
                hasFreeTime = true;
            }
            else if(durationFreeTime>0)
                return new FreeTime(startFreeTime, durationFreeTime);
        }
        return hasFreeTime?new FreeTime(startFreeTime, durationFreeTime):null;
    }

    //Добавление расписания в БД
    public void addStorage()
    {
        storage.setListSchedule(schedules);
    }

    private void setBusyTime(Instant startDate, Instant endDate)
    {
        int iStart = DateConvert.formatDateToIntHHmm(startDate.atZone(UseCase.zone));
        int iEnd = DateConvert.formatDateToIntHHmm(endDate.atZone(UseCase.zone));
        for (int i=iStart; i<iEnd; i++)
            busyTime[i]=true;
    }
}