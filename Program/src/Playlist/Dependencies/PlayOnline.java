package Playlist.Dependencies;

import Playlist.Application.UseCase;
import Playlist.Dependencies.Struct.Ad;
import Playlist.Dependencies.Struct.Music;
import Playlist.Domain.Schedule;
import Playlist.Dependencies.Interface.AdExternalSystem;
import Playlist.Dependencies.Interface.BlackBox;
import Playlist.Dependencies.Interface.MusicExternalSystem;
import Playlist.Dependencies.Interface.Storage;

import java.time.Instant;

import static Playlist.Dependencies.DateConvert.formatBDDateDDMMYYYYHHMM;

public class PlayOnline
{
    private final Storage storage;
    private BlackBox blackBox;
    private AdExternalSystem adExternalSystem;
    private MusicExternalSystem musicExternalSystem;
    private Schedule schedule;

    public PlayOnline(Storage storage, BlackBox blackBox, MusicExternalSystem musicExternalSystem)
    {
        this.storage = storage;
        this.blackBox = blackBox;
        this.musicExternalSystem = musicExternalSystem;
    }

    public PlayOnline(Storage storage, BlackBox blackBox, AdExternalSystem adExternalSystem)
    {
        this.storage = storage;
        this.blackBox = blackBox;
        this.adExternalSystem = adExternalSystem;
    }

    public PlayOnline(Storage storage)
    {
        this.storage = storage;
    }

    public void playAd()
    {
        Ad ad = adExternalSystem.getAd();
        schedule = ad.adToSchedule(0, Instant.now());
        saveSchedule();
    }

    public void playMusic(Music music)
    {
        schedule = music.musicToSchedule(0, Instant.now());
        saveSchedule();
    }

    private void saveSchedule()
    {
        storage.setSchedule(schedule);
        blackBox.playFile(schedule.getHash());
    }

    public int getIdProgramOnline(Instant date)
    {
        return storage.getId(formatBDDateDDMMYYYYHHMM(date.atZone(UseCase.zone)));
    }
}