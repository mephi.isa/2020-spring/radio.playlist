package Playlist.Dependencies;

import Playlist.Application.UseCase;

import javax.jws.soap.SOAPBinding;
import java.time.Instant;
import java.time.ZonedDateTime;

public class DateConvert
{
    //Принимает дату
    //Возвращает формат DD:MM:YYYY
    public static String formatDateToStringDDMMYYYY(ZonedDateTime date)
    {
        String DD = setZeroDateFormat(String.valueOf(date.getDayOfMonth()));
        String MM = setZeroDateFormat(String.valueOf(date.getMonthValue()));
        String YYYY = String.valueOf(date.getYear());
        return DD+":"+MM+":"+YYYY;
    }

    //Принимает дату
    //Возвращает минутную отсучку
    public static int formatDateToIntHHmm(ZonedDateTime date)
    {
        int HH = date.getHour();
        int mm = date.getMinute();
        return 60*HH+mm;
    }

    //Принимает дату начала DD:MM:YYYY 00:00 и минутную отсечку
    //Возвращает дату начала элемента DD:MM:YYYY HH:mm
    public static Instant formatIntToDate(Instant date, int i)
    {
        long dateStartSecond = date.getEpochSecond();
        return Instant.ofEpochSecond(dateStartSecond+i*60);
    }

    //Принимает дату DD:MM:YYYY HH:mm
    //Возвращает время HH:mm
    public static String formatDateToStringHHmm(ZonedDateTime date)
    {
        String HH = setZeroDateFormat(String.valueOf(date.getHour()));
        String mm = setZeroDateFormat(String.valueOf(date.getMinute()));
        return HH+":"+mm;
    }

    //Приводит ZonedDateTime к формату dd-MM-yyyy hh:mm для работы с БД
    public static String formatBDDateDDMMYYYYHHMM(ZonedDateTime date)
    {
        String dd = setZeroDateFormat(String.valueOf(date.getDayOfMonth()));
        String MM = setZeroDateFormat(String.valueOf(date.getMonthValue()));
        String yyyy = String.valueOf(date.getYear());
        String hh = setZeroDateFormat(String.valueOf(date.getHour()));
        String mm = setZeroDateFormat(String.valueOf(date.getMinute()));
        return "to_timestamp('"+dd+"-"+MM+"-"+yyyy+" "+hh+":"+mm+"', 'dd.mm.yyyy hh24:mi')";
    }

    //Приводит ZonedDateTime к формату dd-MM-yyyy для работы с БД
    public static String formatBDDateDDMMYYYY(ZonedDateTime date)
    {
        String dd = setZeroDateFormat(String.valueOf(date.getDayOfMonth()));
        String MM = setZeroDateFormat(String.valueOf(date.getMonthValue()));
        String yyyy = String.valueOf(date.getYear());
        return "to_timestamp('"+dd+"-"+MM+"-"+yyyy+"', 'dd.mm.yyyy')";
    }

    //Добавляет символ 0 для эдементов расписания, которые состоят из одной цифры
    public static String setZeroDateFormat(String elemDate)
    {
        return elemDate.length()==1 ? "0"+elemDate:elemDate;
    }
}
