package Playlist.Enums;

public enum Type
{
    AD
    {
        public String toString()
        {
            return "Реклама";
        }
    },
    MUSIC
    {
        public String toString()
        {
            return "Музыка";
        }
    },
    PROGRAM
    {
        public String toString()
        {
            return "Программа";
        }
    }
}
