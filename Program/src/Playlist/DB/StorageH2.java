package Playlist.DB;

import Playlist.Application.UseCase;
import Playlist.Dependencies.Interface.Storage;
import Playlist.Domain.Schedule;
import Playlist.Enums.Type;

import javax.jws.soap.SOAPBinding;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static Playlist.Dependencies.DateConvert.formatBDDateDDMMYYYYHHMM;

public class StorageH2 implements Storage
{
    private Statement statement;

    public StorageH2()
    {
        try
        {
            statement = AppConfig.getStatement();
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }

    }

    public void setListSchedule(List<Schedule> schedules)
    {
        for(int i=0; i<schedules.size(); i++)
            setSchedule(schedules.get(i));
    }

    public void setSchedule(Schedule schedule)
    {
        String idElem = String.valueOf(schedule.getIdElem());
        String startDate = formatBDDateDDMMYYYYHHMM(schedule.getStartDate().atZone(UseCase.zone));
        String endDate = formatBDDateDDMMYYYYHHMM(schedule.getEndDate().atZone(UseCase.zone));
        String hash = schedule.getHash();
        String type = String.valueOf(schedule.getType().ordinal());

        String sqlValue = "VALUES(" + idElem + ", " + startDate + ", " + endDate + ", '" + hash + "', " + type + ")";
        insertSchedule(sqlValue);
    }

    private void insertSchedule(String sqlValue)
    {
        String sqlInsert = "INSERT INTO SCHEDULE (id_elem, start_date, end_date, hash, type) ";
        try
        {
            statement.executeUpdate(sqlInsert + sqlValue);
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }
    }

    public void updateHashStorage(String id, String hash)
    {
        String sql = "UPDATE SCHEDULE SET hash='"+hash+"' WHERE id="+id;
        try
        {
            statement.executeUpdate(sql);
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }
    }

    public List<Schedule> getSchedules(String date)
    {
        List<Schedule> schedules = new ArrayList<Schedule>();
        String sql = "SELECT * FROM SCHEDULE WHERE to_char(start_date, 'dd:mm:yyyy')='" + date + "'";
        ResultSet rs = selectSchedule(sql);
        try
        {
            while (rs.next())
                schedules.add(getSchedule(rs));
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }
        return schedules;
    }

    private static Schedule getSchedule(ResultSet rs) throws SQLException
    {
        int id  = rs.getInt("id");
        int idElem = rs.getInt("id_elem");
        Type type = Type.valueOf(rs.getString("type"));
        Instant startDate = Instant.ofEpochMilli(rs.getTimestamp("start_date").getTime());
        Instant endDate = Instant.ofEpochMilli(rs.getTimestamp("end_date").getTime());
        String hash = rs.getString("hash");
        return new Schedule(id, idElem, startDate, endDate, hash, type);
    }

    public int getId(String date)
    {
        String sql = "SELECT * FROM SCHEDULE WHERE type='Program' AND start_date<=" + date + " AND end_date>="+date;
        ResultSet rs = selectSchedule(sql);
        try
        {
            if(rs.next())
                return rs.getInt("id");
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }
        return -1;
    }

    public String getHash(String id)
    {
        String sql = "SELECT * FROM SCHEDULE WHERE id=" + id;
        ResultSet rs = selectSchedule(sql);
        try
        {
            if(rs.next())
                return rs.getString("hash");
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }
        return null;
    }

    private ResultSet selectSchedule(String sql)
    {
        try
        {
            return statement.executeQuery(sql);
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }
        return null;
    }
}
