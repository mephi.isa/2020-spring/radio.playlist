package Playlist.DB;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class AppConfig
{
    private static final String JDBC_DRIVER = "org.h2.Driver";
    private static final String DB_URL = "jdbc:h2:./db/db";

    private static final String USER = "HADMIN";
    private static final String PASS = "HADMIN";

    public static Statement getStatement() throws SQLException, ClassNotFoundException
    {
        Class.forName(JDBC_DRIVER);
        return DriverManager.getConnection(DB_URL, USER, PASS).createStatement();
    }
}