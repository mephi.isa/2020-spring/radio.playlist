package Playlist.Exceptions;

public class ScheduleException extends RuntimeException
{
    public static final String errorProgramSetHash = "Для программы нельзя изменить хэш до ее окончания";
    public static final String errorResetHash = "Нельзя переустановить хеш";
    public static final String errorCreateMusic = "Неверно переданы параметры для создания музыкальной композиции";
    public static final String errorCreateAd = "Неверно переданы параметры для создания рекламы";
    public static final String errorCreateProgram = "Неверно переданы параметры для создания передачи";

    public ScheduleException(String message)
    {
        super(message);
    }
}
