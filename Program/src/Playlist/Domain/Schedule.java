package Playlist.Domain;

import Playlist.Enums.Type;
import Playlist.Exceptions.ScheduleException;

import java.time.Instant;

public class Schedule
{
    private int id;
    private int idElem;
    private Instant startDate;
    private Instant endDate;
    private boolean isOnline;
    private String hash;
    private Type type;

    //Для планирования передачи. Продолжительность в секундах
    private Schedule(int id, int idElem, Instant startDate, long duration, boolean isOnline, String hash, Type type)
    {
        this.id = id;
        this.startDate = startDate;
        this.endDate = getEndDate(startDate, duration);
        this.isOnline = isOnline;
        this.idElem = idElem;
        this.hash = hash;
        this.type = type;
    }

    public Schedule(int id, int idElem, Instant startDate, Instant endDate, String hash, Type type)
    {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.idElem = idElem;
        this.hash = hash;
        this.type = type;
    }

    public static Schedule music(int id, int idElem, Instant startDate, long duration, String hash)
    {
        if (startDate==null || hash==null || duration<=0)
            throw new ScheduleException(ScheduleException.errorCreateMusic);
        return new Schedule(id, idElem, startDate, duration, false, hash, Type.MUSIC);
    }

    public static Schedule ad(int id, int idElem, Instant startDate, long duration, String hash)
    {
        if (startDate==null || hash==null || duration<=0)
            throw new ScheduleException(ScheduleException.errorCreateAd);
        return new Schedule(id, idElem, startDate, duration, false, hash, Type.AD);
    }

    public static Schedule ad(int id, int idElem, Instant startDate, long duration)
    {
        if (startDate==null || duration<=0)
            throw new ScheduleException(ScheduleException.errorCreateAd);
        return new Schedule(id, idElem, startDate, duration, false, null, Type.AD);
    }

    public static Schedule program(int id, int idElem, Instant startDate, long duration)
    {
        if (startDate==null || duration<=0)
            throw new ScheduleException(ScheduleException.errorCreateProgram);
        return new Schedule(id, idElem, startDate, duration, true, null, Type.PROGRAM);
    }

    //Вычисление продолжительности в секундах
    public long duration()
    {
        return this.endDate.getEpochSecond()-this.startDate.getEpochSecond();
    }

    //Определение наличия хеша в системе
    public boolean hasHash()
    {
        return this.hash!=null;
    }

    public void setHash(String hash)
    {
        if (this.type==Type.PROGRAM && this.endDate.getEpochSecond()>=Instant.now().getEpochSecond())
            throw new ScheduleException(ScheduleException.errorProgramSetHash);
        if (this.hasHash())
            throw new ScheduleException(ScheduleException.errorResetHash);
        this.hash = hash;
    }

    public int getId()
    {
        return this.id;
    }

    public int getIdElem()
    {
        return this.idElem;
    }

    public Instant getStartDate()
    {
        return this.startDate;
    }

    public Instant getEndDate()
    {
        return this.endDate;
    }

    public String getHash()
    {
        return this.hash;
    }

    public Type getType()
    {
        return this.type;
    }

    //Вычисление даты окончания через дату начала и продолжительнность в секундах
    public static Instant getEndDate(Instant startDate, long duration)
    {
        return Instant.ofEpochSecond(startDate.getEpochSecond()+duration);
    }

    //Вычисление даты окончания через дату начала и продолжительнность в секундах
    public static long getDuration(Instant startDate, Instant endDate)
    {
        return endDate.getEpochSecond()-startDate.getEpochSecond();
    }
}
