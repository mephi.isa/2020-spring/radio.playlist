package Playlist.Application;

import Playlist.Dependencies.*;
import Playlist.Dependencies.Struct.Music;
import Playlist.Dependencies.Struct.ScheduleSimple;
import Playlist.Dependencies.Interface.*;

import java.time.Instant;
import java.time.ZoneId;
import java.util.List;

import static Playlist.Dependencies.DateConvert.formatBDDateDDMMYYYYHHMM;

public class UseCase
{
    public static final ZoneId zone = ZoneId.of("Europe/Moscow");
    //public static final ZoneId zoneZero = ZoneId.ofOffset("UTC", ZoneOffset.ofHours(0));
    private Storage storage;
    private BlackBox blackBox;
    private AdExternalSystem adExternalSystem;
    private MusicExternalSystem musicExternalSystem;
    private ProgramExternalSystem programExternalSystem;

    public UseCase(Storage storage, BlackBox blackBox, AdExternalSystem adExternalSystem, MusicExternalSystem musicExternalSystem, ProgramExternalSystem programExternalSystem)
    {
        this.storage = storage;
        this.blackBox = blackBox;
        this.adExternalSystem = adExternalSystem;
        this.musicExternalSystem = musicExternalSystem;
        this.programExternalSystem = programExternalSystem;
    }

    public void generateSchedule(Instant date)
    {
        GenerateSchedule gs = new GenerateSchedule(storage, adExternalSystem, musicExternalSystem, programExternalSystem, date);
        gs.generateSchedule();
        gs.addStorage();
    }

    public List<ScheduleSimple> viewSchedule(Instant date)
    {
        ViewSchedule vs = new ViewSchedule(storage, date);
        return vs.viewSchedule();
    }

    //запуск музки в прямом эфире
    public void playMusicOnline(Music music)
    {
        PlayOnline playOnline = new PlayOnline(storage, blackBox, musicExternalSystem);
        playOnline.playMusic(music);
    }

    //зупуск рекламы в прямом эфире
    public void playAdOnline()
    {
        PlayOnline playOnline = new PlayOnline(storage, blackBox, adExternalSystem);
        playOnline.playAd();
    }

    public List<Music> viewMusicListForPlayOnline()
    {
        return musicExternalSystem.getMusics();
    }

    public void playMusic(int id)
    {
        String hash = storage.getHash(String.valueOf(id));
        blackBox.playFile(hash);
    }

    //устанавливает хеш при воспроизведении и воспроизводит файл
    public void playAd(int id)
    {
        String hash = adExternalSystem.getHash(id);
        storage.updateHashStorage(String.valueOf(id), hash);
        blackBox.playFile(hash);
    }

    public int startLiveBroadcast(Instant date)
    {
        PlayOnline playOnline = new PlayOnline(storage);
        int idProgram = playOnline.getIdProgramOnline(date);
        if (idProgram!=-1)
            blackBox.startOnline();
        return idProgram;
    }

    //завершение эфира и устанавливание хэш программы
    public void endLiveBroadcast(int id)
    {
        String hash = blackBox.endOnline();
        storage.updateHashStorage(String.valueOf(id), hash);
    }
}