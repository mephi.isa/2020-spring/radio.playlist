package Playlist.Tests;

import Playlist.Domain.Schedule;
import Playlist.Enums.Type;
import Playlist.Exceptions.ScheduleException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class ScheduleTest {

    private Schedule music;
    private Schedule ad;
    private Schedule program;

    @BeforeEach
    void setBeforeTest()
    {
        music = null;
        ad = null;
        program = null;
    }

    //Создаем музыкальную композицию, которая упадет с ошибкой
    @Test
    void createMusicError()
    {
        Throwable thrown = assertThrows(ScheduleException.class, () ->
        {
            music = Schedule.music(1, 17, null, 96, "test");
        });
        assertEquals(ScheduleException.errorCreateMusic, thrown.getMessage());
        assertNull(music);

        thrown = assertThrows(ScheduleException.class, () ->
        {
            music = Schedule.music(1, 17, Instant.parse("2020-05-10T10:15:30Z"), -120, "test");
        });
        assertEquals(ScheduleException.errorCreateMusic, thrown.getMessage());
        assertNull(music);

        thrown = assertThrows(ScheduleException.class, () ->
        {
            music = Schedule.music(1, 17, Instant.parse("2020-05-10T10:15:30Z"), 125, null);
        });
        assertEquals(ScheduleException.errorCreateMusic, thrown.getMessage());
        assertNull(music);
    }

    //Создаем музыкальную композицию
    @Test
    void createMusic()
    {
        music = Schedule.music(1, 17, Instant.parse("2020-05-10T10:15:30Z"), 178, "test");
        assertEquals(music.getId(), 1);
        assertEquals(music.getIdElem(), 17);
        assertTrue(Instant.parse("2020-05-10T10:15:30Z").equals(music.getStartDate()));
        assertTrue(Instant.parse("2020-05-10T10:18:28Z").equals(music.getEndDate()));
        assertEquals(music.duration(), 178);
        assertTrue(music.hasHash());
        assertEquals(music.getHash(), "test");
        assertEquals(Type.MUSIC, music.getType());
    }

    //Создаем рекламу с хешем, которая упадет с ошибкой
    @Test
    void createAdWithHashError()
    {
        Throwable thrown = assertThrows(ScheduleException.class, () ->
        {
            ad = Schedule.ad(5, 10, null, 654, "test");
        });
        assertEquals(ScheduleException.errorCreateAd, thrown.getMessage());
        assertNull(ad);

        thrown = assertThrows(ScheduleException.class, () ->
        {
            ad = Schedule.ad(1, 17, Instant.parse("2008-05-10T10:15:30Z"), -10, "test");
        });
        assertEquals(ScheduleException.errorCreateAd, thrown.getMessage());
        assertNull(ad);

        thrown = assertThrows(ScheduleException.class, () ->
        {
            ad = Schedule.ad(1, 17, Instant.parse("2019-05-10T10:15:30Z"), 176, null);
        });
        assertEquals(ScheduleException.errorCreateAd, thrown.getMessage());
        assertNull(ad);
    }

    //Создаем рекламу с хешем
    @Test
    void createAdWithHash()
    {
        ad = Schedule.ad(5, 88, Instant.parse("2020-05-10T10:15:30Z"), 18, "test");
        assertEquals(ad.getId(), 5);
        assertEquals(ad.getIdElem(), 88);
        assertTrue(Instant.parse("2020-05-10T10:15:30Z").equals(ad.getStartDate()));
        assertTrue(Instant.parse("2020-05-10T10:15:48Z").equals(ad.getEndDate()));
        assertEquals(ad.duration(), 18);
        assertTrue(ad.hasHash());
        assertEquals(ad.getHash(), "test");
        assertEquals(Type.AD, ad.getType());
    }

    //Создаем рекламу без хеша, которая упадет с ошибкой
    @Test
    void createAdWithoutHashError()
    {
        Throwable thrown = assertThrows(ScheduleException.class, () ->
        {
            ad = Schedule.ad(5, 10, null, 654);
        });
        assertEquals(ScheduleException.errorCreateAd, thrown.getMessage());
        assertNull(ad);

        thrown = assertThrows(ScheduleException.class, () ->
        {
            ad = Schedule.ad(1, 17, Instant.parse("2008-05-10T10:15:30Z"), -10);
        });
        assertEquals(ScheduleException.errorCreateAd, thrown.getMessage());
        assertNull(ad);
    }

    //Создаем рекламу без хеша
    @Test
    void createAdWithoutHash()
    {
        ad = Schedule.ad(5, 88, Instant.parse("2020-05-10T10:15:30Z"), 128);
        assertEquals(ad.getId(), 5);
        assertEquals(ad.getIdElem(), 88);
        assertTrue(Instant.parse("2020-05-10T10:15:30Z").equals(ad.getStartDate()));
        assertTrue(Instant.parse("2020-05-10T10:17:38Z").equals(ad.getEndDate()));
        assertEquals(ad.duration(), 128);
        assertFalse(ad.hasHash());
        assertNull(ad.getHash());
        assertEquals(Type.AD, ad.getType());
    }

    //Создаем передачу, которая упадет с ошибкой
    @Test
    void createProgramError()
    {
        Throwable thrown = assertThrows(ScheduleException.class, () ->
        {
            program = Schedule.program(5, 10, null, 654);
        });
        assertEquals(ScheduleException.errorCreateProgram, thrown.getMessage());
        assertNull(program);

        thrown = assertThrows(ScheduleException.class, () ->
        {
            program = Schedule.program(1, 17, Instant.parse("2008-05-10T10:15:30Z"), -100);
        });
        assertEquals(ScheduleException.errorCreateProgram, thrown.getMessage());
        assertNull(program);
    }

    //Создаем передачу
    @Test
    void createProgramHash()
    {
        program = Schedule.program(65, 8, Instant.parse("2020-05-10T10:15:30Z"), 308);
        assertEquals(program.getId(), 65);
        assertEquals(program.getIdElem(), 8);
        assertTrue(Instant.parse("2020-05-10T10:15:30Z").equals(program.getStartDate()));
        assertTrue(Instant.parse("2020-05-10T10:20:38Z").equals(program.getEndDate()));
        assertEquals(program.duration(), 308);
        assertFalse(program.hasHash());
        assertNull(program.getHash());
        assertEquals(Type.PROGRAM, program.getType());
    }

    //Создаем музыкальную композицию и устанавливает ей новый хеш
    @Test
    void createMusicSetHash()
    {
        Schedule m = Schedule.music(1, 17, Instant.parse("2020-05-10T10:15:30Z"), 96, "test");
        Throwable thrown = assertThrows(ScheduleException.class, () ->
        {
            m.setHash("test-new");
        });
        assertEquals(ScheduleException.errorResetHash, thrown.getMessage());
        assertEquals(m.getHash(), "test");
    }

    //Создаем программу и устанавливает ей новый хеш до окончания передачи
    @Test
    void createProgramSetHashBeforeEndDate()
    {
        Schedule pr = Schedule.program(1, 17, Instant.now(), 18000);
        Throwable thrown = assertThrows(ScheduleException.class, () ->
        {
            pr.setHash("test-new");
        });
        assertEquals(ScheduleException.errorProgramSetHash, thrown.getMessage());
        assertNull(pr.getHash());
    }

    //Создаем программу и устанавливает ей новый хеш после окончания передачи
    @Test
    void createProgramSetHashAfterEndDate()
    {
        Schedule pr = Schedule.program(1, 17, Instant.parse("2010-05-10T10:15:30Z"), 96);
        pr.setHash("test-new");
        assertEquals(pr.getHash(), "test-new");
    }

    //Создаем программу и переустанавливаем ей хеш
    @Test
    void createProgramResetHash()
    {
        Schedule pr = Schedule.program(1, 17, Instant.parse("2010-05-10T10:15:30Z"), 96);
        pr.setHash("test-new");
        assertEquals(pr.getHash(), "test-new");
        Throwable thrown = assertThrows(ScheduleException.class, () ->
        {
            pr.setHash("test-new-new");
        });
        assertEquals(ScheduleException.errorResetHash, thrown.getMessage());
        assertEquals(pr.getHash(), "test-new");
    }

    //Создаем рекламу изначально с хешем и переустанавливаем ей хеш
    @Test
    void createAdWithHashResetHash()
    {
        Schedule a = Schedule.ad(1, 17, Instant.parse("2010-05-10T10:15:30Z"), 96, "test");
        Throwable thrown = assertThrows(ScheduleException.class, () ->
        {
            a.setHash("test-new");
        });
        assertEquals(ScheduleException.errorResetHash, thrown.getMessage());
        assertEquals(a.getHash(), "test");
    }

    //Создаем рекламу без хеша и переустанавливаем ей хеш
    @Test
    void createAdWithoutHashResetHash()
    {
        Schedule a = Schedule.ad(1, 17, Instant.parse("2010-05-10T10:15:30Z"), 96);
        a.setHash("test-new");
        assertEquals(a.getHash(), "test-new");
        Throwable thrown = assertThrows(ScheduleException.class, () ->
        {
            a.setHash("test-new-new");
        });
        assertEquals(ScheduleException.errorResetHash, thrown.getMessage());
        assertEquals(a.getHash(), "test-new");
    }

    //Проверка статической функции
    @Test
    void getEndDate()
    {
        Instant endDateExpected = Schedule.getEndDate(Instant.parse("2020-05-10T10:15:30Z"), 67);
        Instant endDateActual = Instant.parse("2020-05-10T10:16:37Z");
        assertTrue(endDateActual.equals(endDateExpected));
    }

    @Test
    void getDuration()
    {
        long durationExpected = Schedule.getDuration(Instant.parse("2020-05-10T10:15:30Z"), Instant.parse("2020-05-10T10:16:37Z"));
        long durationActual = 67;
        assertEquals(durationExpected, durationActual);
    }
}