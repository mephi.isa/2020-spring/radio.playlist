package Playlist.Tests;

import Playlist.Dependencies.Struct.Music;
import Playlist.Domain.Schedule;
import Playlist.Enums.Type;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class MusicTest {

    @Test
    void musicToSchedule()
    {
        int idElemExpected = 88;
        int durationExpected = 47;
        int idExpected = 0;
        String hashExpected = "10333";
        Instant startDateExpected = Instant.parse("2020-05-10T00:00:00Z");
        Music music = new Music(idElemExpected, durationExpected, hashExpected);
        Schedule schedule = music.musicToSchedule(idExpected, startDateExpected);
        assertEquals(idExpected, schedule.getId());
        assertEquals(idElemExpected, schedule.getIdElem());
        assertEquals(durationExpected, schedule.duration());
        assertTrue(startDateExpected.equals(schedule.getStartDate()));
        assertNotNull(schedule.getHash());
        assertEquals(hashExpected, schedule.getHash());
        assertEquals(Type.MUSIC, schedule.getType());
    }
}