package Playlist.Tests;

import Playlist.Dependencies.Struct.Ad;
import Playlist.Domain.Schedule;
import Playlist.Enums.Type;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class AdTest {

    @Test
    void adToScheduleWithHash()
    {
        int idElemExpected = 88;
        int durationExpected = 47;
        int idExpected = 0;
        Instant startDateExpected = Instant.parse("2020-05-10T00:00:00Z");
        Ad ad = new Ad(idElemExpected, 1, durationExpected);
        Schedule schedule = ad.adToSchedule(idExpected, startDateExpected);
        assertEquals(idExpected, schedule.getId());
        assertEquals(idElemExpected, schedule.getIdElem());
        assertEquals(durationExpected, schedule.duration());
        assertTrue(startDateExpected.equals(schedule.getStartDate()));
        assertNull(schedule.getHash());
        assertEquals(Type.AD, schedule.getType());
    }

    @Test
    void adToScheduleWithoutHash()
    {
        int idElemExpected = 88;
        int durationExpected = 47;
        int idExpected = 0;
        String hashExpected = "10333";
        Instant startDateExpected = Instant.parse("2020-05-10T00:00:00Z");
        Ad ad = new Ad(idElemExpected, durationExpected, hashExpected);
        Schedule schedule = ad.adToSchedule(idExpected, startDateExpected);
        assertEquals(idExpected, schedule.getId());
        assertEquals(idElemExpected, schedule.getIdElem());
        assertEquals(durationExpected, schedule.duration());
        assertTrue(startDateExpected.equals(schedule.getStartDate()));
        assertNotNull(schedule.getHash());
        assertEquals(hashExpected, schedule.getHash());
        assertEquals(Type.AD, schedule.getType());
    }
}