package Playlist.Tests;

import Playlist.Dependencies.DateConvert;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.*;

class DateConvertTest {

    @Test
    void formatDateToStringDDMMYYYYWithoutZero()
    {
        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2007-12-23T10:15:30+01:00[Europe/Moscow]");
        String expected = "23:12:2007";
        String actual = DateConvert.formatDateToStringDDMMYYYY(zonedDateTime);
        assertEquals(expected, actual);
    }

    @Test
    void formatDateToStringDDMMYYYYWithZero()
    {
        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2007-02-03T10:15:30+03:00[Europe/Moscow]");
        String expected = "03:02:2007";
        String actual = DateConvert.formatDateToStringDDMMYYYY(zonedDateTime);
        assertEquals(expected, actual);
    }

    @Test
    void formatDateToIntHHmm()
    {
        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2007-02-03T10:15:30+03:00[Europe/Moscow]");
        int expected = 615;
        int actual = DateConvert.formatDateToIntHHmm(zonedDateTime);
        assertEquals(expected, actual);
    }

    @Test
    void formatIntToDate()
    {
        Instant date = Instant.parse("2020-05-10T00:00:00Z");
        int minute = 121;
        Instant expected = Instant.parse("2020-05-10T02:01:00Z");
        Instant actual = DateConvert.formatIntToDate(date, minute);
        assertEquals(expected.getEpochSecond(), actual.getEpochSecond());
    }

    @Test
    void formatBDDateDDMMYYYYHHMMWithZero()
    {
        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2007-02-03T01:05:30+03:00[Europe/Moscow]");
        String expected = "to_timestamp('03-02-2007 01:05', 'dd.mm.yyyy hh24:mi')";
        String actual = DateConvert.formatBDDateDDMMYYYYHHMM(zonedDateTime);
        assertEquals(expected, actual);
    }

    @Test
    void formatBDDateDDMMYYYYHHMMWithoutZero()
    {
        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2007-12-13T10:15:30+03:00[Europe/Moscow]");
        String expected = "to_timestamp('13-12-2007 10:15', 'dd.mm.yyyy hh24:mi')";
        String actual = DateConvert.formatBDDateDDMMYYYYHHMM(zonedDateTime);
        assertEquals(expected, actual);
    }

    @Test
    void formatDateToStringHHmmWithoutZero()
    {
        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2007-02-03T11:33:30+03:00[Europe/Moscow]");
        String expected = "11:33";
        String actual = DateConvert.formatDateToStringHHmm(zonedDateTime);
        assertEquals(expected, actual);
    }

    @Test
    void formatDateToStringHHmmWithZero()
    {
        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2007-02-03T01:07:30+03:00[Europe/Moscow]");
        String expected = "01:07";
        String actual = DateConvert.formatDateToStringHHmm(zonedDateTime);
        assertEquals(expected, actual);
    }

    @Test
    void formatBDDateDDMMYYYYWithoutZero()
    {
        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2007-12-13T01:07:30+03:00[Europe/Moscow]");
        String expected = "to_timestamp('13-12-2007', 'dd.mm.yyyy')";
        String actual = DateConvert.formatBDDateDDMMYYYY(zonedDateTime);
        assertEquals(expected, actual);
    }

    @Test
    void formatBDDateDDMMYYYYWithZero()
    {
        ZonedDateTime zonedDateTime = ZonedDateTime.parse("2007-02-03T01:07:30+03:00[Europe/Moscow]");
        String expected = "to_timestamp('03-02-2007', 'dd.mm.yyyy')";
        String actual = DateConvert.formatBDDateDDMMYYYY(zonedDateTime);
        assertEquals(expected, actual);
    }
}