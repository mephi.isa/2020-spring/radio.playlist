package Playlist.Tests;

import Playlist.Dependencies.Struct.ScheduleSimple;
import Playlist.Domain.Schedule;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static Playlist.Dependencies.Struct.ScheduleSimple.scheduleToScheduleSimple;
import static org.junit.jupiter.api.Assertions.*;

class ScheduleSimpleTest
{
    @Test
    void scheduleAdWithHashToScheduleSimple()
    {
        Schedule ad = Schedule.ad(12, 14, Instant.parse("2020-01-01T21:15:30Z"), 210, "1234"); //учитываем 3 часа
        ScheduleSimple scheduleSimple = scheduleToScheduleSimple(ad);
        assertEquals("Реклама", scheduleSimple.getType());
        assertEquals("00:15", scheduleSimple.getStartTime());
        assertEquals("00:19", scheduleSimple.getEndTime());
    }

    @Test
    void scheduleAdWithoutHashToScheduleSimple()
    {
        Schedule ad = Schedule.ad(12, 14, Instant.parse("2020-05-15T18:59:30Z"), 60); //учитываем 3 часа
        ScheduleSimple scheduleSimple = scheduleToScheduleSimple(ad);
        assertEquals("Реклама", scheduleSimple.getType());
        assertEquals("21:59", scheduleSimple.getStartTime());
        assertEquals("22:00", scheduleSimple.getEndTime());
    }

    @Test
    void scheduleMusicToScheduleSimple()
    {
        Schedule music = Schedule.music(12, 14, Instant.parse("2021-05-17T13:15:30Z"), 330, "345"); //учитываем 3 часа
        ScheduleSimple scheduleSimple = scheduleToScheduleSimple(music);
        assertEquals("Музыка", scheduleSimple.getType());
        assertEquals("16:15", scheduleSimple.getStartTime());
        assertEquals("16:21", scheduleSimple.getEndTime());
    }

    @Test
    void scheduleProgramToScheduleSimple()
    {
        Schedule program = Schedule.program(12, 14, Instant.parse("2020-05-10T07:15:30Z"), 145); //учитываем 3 часа
        ScheduleSimple scheduleSimple = scheduleToScheduleSimple(program);
        assertEquals("Программа", scheduleSimple.getType());
        assertEquals("10:15", scheduleSimple.getStartTime());
        assertEquals("10:17", scheduleSimple.getEndTime());
    }
}