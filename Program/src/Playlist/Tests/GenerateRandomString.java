package Playlist.Tests;

import java.util.Random;

public class GenerateRandomString
{
    private static final String CHAR_LIST = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static String generateRandomStringUsingSecureRandom()
    {
        Random random = new Random();
        int length = random.nextInt(15)+5;
        StringBuffer randStr = new StringBuffer(length);
        for( int i = 0; i < length; i++)
            randStr.append(CHAR_LIST.charAt(random.nextInt(CHAR_LIST.length())));
        return randStr.toString();
    }
}
