package Playlist.Tests;

import Playlist.DB.StorageH2;
import Playlist.Domain.Schedule;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.List;

class StorageH2Test
{
    @Test
    void setScheduleMusic()
    {
        StorageH2 storageH2 = new StorageH2();
        Schedule schedule = Schedule.music(0, 17, Instant.parse("2020-05-10T10:15:30Z"), 178, "test");
        storageH2.setSchedule(schedule);
    }

    @Test
    void setScheduleProgram()
    {
        StorageH2 storageH2 = new StorageH2();
        Schedule schedule = Schedule.program(0, 19, Instant.now(), 7200);
        storageH2.setSchedule(schedule);
    }

    @Test
    void setScheduleAdWithHash()
    {
        StorageH2 storageH2 = new StorageH2();
        Schedule schedule = Schedule.ad(0, 10, Instant.parse("2020-05-10T17:45:30Z"), 600);
        storageH2.setSchedule(schedule);
    }

    @Test
    void setScheduleAdWithoutHash()
    {
        StorageH2 storageH2 = new StorageH2();
        Schedule schedule = Schedule.ad(0, 11, Instant.parse("2020-12-15T10:35:30Z"), 360, "test");
        storageH2.setSchedule(schedule);
    }

    @Test
    void getIdBetween()
    {
        String expectedDate = "to_timestamp('01-01-2000 00:30', 'dd.mm.yyyy hh24:mi')";
        StorageH2 storageH2 = new StorageH2();
        int id = storageH2.getId(expectedDate);
        System.out.println(id);
    }

    @Test
    void getIdStart()
    {
        String expectedDate = "to_timestamp('01-01-2000 00:00', 'dd.mm.yyyy hh24:mi')";
        StorageH2 storageH2 = new StorageH2();
        int id = storageH2.getId(expectedDate);
        System.out.println(id);
    }

    @Test
    void getIdEnd()
    {
        String expectedDate = "to_timestamp('01-01-2000 01:00', 'dd.mm.yyyy hh24:mi')";
        StorageH2 storageH2 = new StorageH2();
        int id = storageH2.getId(expectedDate);
        System.out.println(id);
    }

    @Test
    void getIdAfter()
    {
        String expectedDate = "to_timestamp('01-01-2000 04:00', 'dd.mm.yyyy hh24:mi')";
        StorageH2 storageH2 = new StorageH2();
        int id = storageH2.getId(expectedDate);
        System.out.println(id);
    }

    @Test
    void getIdBefore()
    {
        String expectedDate = "to_timestamp('31-12-1999 22:00', 'dd.mm.yyyy hh24:mi')";
        StorageH2 storageH2 = new StorageH2();
        int id = storageH2.getId(expectedDate);
        System.out.println(id);
    }

    @Test
    void getSchedules()
    {
        String expectedDate = "12:06:2020";
        StorageH2 storageH2 = new StorageH2();
        List<Schedule> schedules = storageH2.getSchedules(expectedDate);
        for (int i=0; i<schedules.size(); i++)
        {
            System.out.println(schedules.get(i).getId() + " " + schedules.get(i).getHash());

        }
    }

    @Test
    void updateHashStorage()
    {
        String expectedHash = "test_upd";
        String id = "10";
        StorageH2 storageH2 = new StorageH2();
        storageH2.updateHashStorage(id, expectedHash);
    }
}