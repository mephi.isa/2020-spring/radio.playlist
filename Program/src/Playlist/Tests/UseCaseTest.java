package Playlist.Tests;

import Playlist.Application.UseCase;
import Playlist.Dependencies.Interface.*;
import Playlist.Dependencies.Struct.Ad;
import Playlist.Dependencies.Struct.Music;
import Playlist.Dependencies.Struct.Program;
import Playlist.Dependencies.Struct.ScheduleSimple;
import Playlist.Domain.Schedule;
import Playlist.Enums.Type;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UseCaseTest
{
    private MusicExternalSystemSpy musicExternalSystemSpy;
    private MusicExternalSystemEmptySpy musicExternalSystemEmptySpy;
    private AdExternalSystemSpy adExternalSystemSpy;
    private ProgramExternalSystemSpy programExternalSystemSpy;
    private StorageSpy storageSpy;
    private BlackBoxSpy blackBoxSpy;
    private UseCase useCase;

    private class ProgramExternalSystemSpy implements ProgramExternalSystem
    {
        public String date;

        public List<Program> getPrograms(String date)
        {
            this.date = date;
            List<Program> programs = new ArrayList<Program>(2);
            if (date.equals("10:05:2020"))
            {
                programs.add(new Program(56, 7200, Instant.parse("2020-05-10T12:00:00Z"))); //учитываем 3 часа
                programs.add(new Program(6, 6600, Instant.parse("2020-05-10T15:00:00Z"))); //учитываем 3 часа
            }
            else if(date.equals("01:01:2020"))
            {
                programs.add(new Program(3, 1800, Instant.parse("2020-01-01T00:00:00Z"))); //учитываем 3 часа
                programs.add(new Program(9, 7200, Instant.parse("2020-01-01T04:00:00Z"))); //учитываем 3 часа
            }
            return programs;
        }
    }

    private class MusicExternalSystemSpy implements MusicExternalSystem
    {
        public int id;
        public Music music;
        public List<Integer> durationFreeTimeList = new ArrayList<Integer>(4);

        public List<Music> getMusics(int durationFreeTime)
        {
            durationFreeTimeList.add(durationFreeTime);
            List <Music> musics = new ArrayList<Music>(5);
            if (durationFreeTime==1440)
            {
                musics.add(new Music(4, 184, "q-1"));
                musics.add(new Music(6, 134, "q-2"));
                musics.add(new Music(9, 176, "q-4"));
                musics.add(new Music(13, 250, "q-5"));
                musics.add(new Music(17, 145, "q-3"));
            }
            else if (durationFreeTime==176)
            {
                musics.add(new Music(41, 184, "w-1"));
                musics.add(new Music(61, 134, "w-2"));
                musics.add(new Music(19, 176, "w-4"));
            }
            else if (durationFreeTime==420)
            {
                musics.add(new Music(24, 250, "u-1"));
                musics.add(new Music(26, 300, "u-2"));
            }
            else if (durationFreeTime==475)
            {
                musics.add(new Music(117, 600, "a-3"));
            }
            return musics;
        }

        public List<Music> getMusics()
        {
            List <Music> musics = new ArrayList<Music>(2);
            musics.add(new Music(2, 301, "p-1", "Smells Like Teen Spirit", "Nirvana"));
            musics.add(new Music(22, 121, "p-2", "We Will Rock You", "Queen"));
            return musics;
        }
    }

    private class MusicExternalSystemEmptySpy implements MusicExternalSystem
    {
        public List<Integer> durationFreeTimeList = new ArrayList<Integer>(7);

        public List<Music> getMusics(int durationFreeTime)
        {
            durationFreeTimeList.add(durationFreeTime);
            return null;
        }

        public List<Music> getMusics()
        {
            return null;
        }
    }

    private class StorageSpy implements Storage
    {
        public Schedule schedule;
        public String id;
        public String hash;
        public List<Schedule> schedules;
        public String date;

        public void setListSchedule(List<Schedule> schedules)
        {
            this.schedules = new ArrayList<Schedule>(schedules);
            for (int i=0; i<schedules.size(); i++)
                setSchedule(schedules.get(i));
        }

        public void setSchedule(Schedule schedule)
        {
            this.schedule = schedule;
        }

        public void updateHashStorage(String id, String hash)
        {
            this.id = id;
            this.hash = hash;
        }

        public List<Schedule> getSchedules(String date)
        {
            this.date = date;
            this.schedules = new ArrayList<Schedule>(4);
            if (date.equals("02:02:2020"))
            {
                schedules.add(Schedule.music(13, 88, Instant.parse("2020-02-01T22:00:00Z"), 468, "a-1")); //учитываем 3 часа
                schedules.add(Schedule.program(14, 12, Instant.parse("2020-02-02T06:00:00Z"), 7200)); //учитываем 3 часа
                schedules.add(Schedule.ad(15, 12, Instant.parse("2020-02-02T15:00:00Z"), 600)); //учитываем 3 часа
                schedules.add(Schedule.ad(16, 8, Instant.parse("2020-02-02T17:00:00Z"), 300, "qwer31")); //учитываем 3 часа
            }
            return schedules;
        }

        public String getHash(String id)
        {
            this.id = id;
            return "1234";
        }

        public int getId(String date)
        {
            this.date = date;
            if (date.equals("to_timestamp('13-12-2007 10:15', 'dd.mm.yyyy hh24:mi')"))
                return 14;
            return -1;
        }

    }

    private class AdExternalSystemSpy implements AdExternalSystem
    {
        public int id;
        public Ad ad;
        public String date;

        public List<Ad> getAds(String date)
        {
            List<Ad> ads = new ArrayList<Ad>(4);
            this.date = date;
            if(date.equals("04:04:2020"))
            {
                ads.add(new Ad(2, 0, 120));
            }
            else if(date.equals("05:04:2020"))
            {
                ads.add(new Ad(12, 1, 120));
                ads.add(new Ad(112, 1, 300));
                ads.add(new Ad(1, 1, 180));
                ads.add(new Ad(88, 1, 120));
            }
            else if(date.equals("06:04:2020"))
            {
                ads.add(new Ad(22, 2, 120));
                ads.add(new Ad(342, 2, 300));
                ads.add(new Ad(5, 3, 180));
            }
            else if(date.equals("01:01:2020"))
            {
                ads.add(new Ad(25, 1, 240));
                ads.add(new Ad(250, 2, 300));
            }
            return ads;
        }

        public Ad getAd()
        {
            ad = new Ad(5, 130, "4334");
            return ad;
        }

        public String getHash(int id)
        {
            this.id = id;
            return "4444";
        }
    }

    private class BlackBoxSpy implements BlackBox
    {
        public String hash;
        public boolean flagOnline;

        public void playFile(String hash)
        {
            this.hash = hash;
        }

        public void startOnline()
        {
            flagOnline = true;
        }

        public String endOnline()
        {
            flagOnline = false;
            return "qwerty";
        }
    }

    @BeforeEach
    void setInterface()
    {
        musicExternalSystemEmptySpy = new MusicExternalSystemEmptySpy();
        musicExternalSystemSpy = new MusicExternalSystemSpy();
        adExternalSystemSpy = new AdExternalSystemSpy();
        programExternalSystemSpy = new ProgramExternalSystemSpy();
        storageSpy = new StorageSpy();
        blackBoxSpy = new BlackBoxSpy();
        useCase = new UseCase(storageSpy, blackBoxSpy, adExternalSystemSpy, musicExternalSystemSpy, programExternalSystemSpy);
    }

    @Test
    void playMusicOnline()
    {
        long startTimeTest = Instant.now().getEpochSecond();
        Music music = new Music(88, 122, "8888");
        useCase.playMusicOnline(music);
        long endTimeTest = Instant.now().getEpochSecond();
        assertEquals("8888", blackBoxSpy.hash);
        assertEquals(122, storageSpy.schedule.duration());
        assertEquals("8888", storageSpy.schedule.getHash());
        assertEquals(88, storageSpy.schedule.getIdElem());
        assertEquals(Type.MUSIC, storageSpy.schedule.getType());
        long actualTime = storageSpy.schedule.getStartDate().getEpochSecond();
        assertTrue(startTimeTest<=actualTime && endTimeTest>=actualTime);
    }

    @Test
    void playAdOnline()
    {
        long startTimeTest = Instant.now().getEpochSecond();
        useCase.playAdOnline();
        long endTimeTest = Instant.now().getEpochSecond();
        assertEquals("4334", blackBoxSpy.hash);
        assertEquals(130, storageSpy.schedule.duration());
        assertEquals("4334", storageSpy.schedule.getHash());
        assertEquals(5, storageSpy.schedule.getIdElem());
        assertEquals(Type.AD, storageSpy.schedule.getType());
        long actualTime = storageSpy.schedule.getStartDate().getEpochSecond();
        assertTrue(startTimeTest<=actualTime && endTimeTest>=actualTime);
    }

    @Test
    void playMusic()
    {
        int id = 94;
        useCase.playMusic(id);
        assertEquals(String.valueOf(id), storageSpy.id);
        assertEquals("1234", blackBoxSpy.hash);
    }

    @Test
    void playAd()
    {
        int id = 55;
        useCase.playAd(id);
        assertEquals(id, adExternalSystemSpy.id);
        assertEquals(String.valueOf(id), storageSpy.id);
        assertEquals("4444", storageSpy.hash);
        assertEquals("4444", blackBoxSpy.hash);
    }

    @Test
    void generateScheduleEmpty()
    {
        UseCase useCase = new UseCase(storageSpy, blackBoxSpy, adExternalSystemSpy, musicExternalSystemEmptySpy, programExternalSystemSpy);
        Instant date = Instant.parse("2020-05-10T21:00:00Z"); //учитываем 3 часа
        useCase.generateSchedule(date);

        assertEquals("11:05:2020", programExternalSystemSpy.date);
        assertEquals("11:05:2020", adExternalSystemSpy.date);

        //Проверка МК
        assertEquals(1, musicExternalSystemEmptySpy.durationFreeTimeList.size());
        assertEquals(1440, musicExternalSystemEmptySpy.durationFreeTimeList.get(0));

        //Проверка хранилища
        assertEquals(0, storageSpy.schedules.size());
    }

    @Test
    void generateScheduleOnlyProgram()
    {
        MusicExternalSystemEmptySpy musicExternalSystemEmptySpy = new MusicExternalSystemEmptySpy();
        UseCase useCase = new UseCase(storageSpy, blackBoxSpy, adExternalSystemSpy, musicExternalSystemEmptySpy, programExternalSystemSpy);
        Instant date = Instant.parse("2020-05-09T21:00:00Z"); //учитываем 3 часа
        useCase.generateSchedule(date);

        assertEquals("10:05:2020", programExternalSystemSpy.date);
        assertEquals("10:05:2020", adExternalSystemSpy.date);

        System.out.println();

        //Проверка МК
        assertEquals(3, musicExternalSystemEmptySpy.durationFreeTimeList.size());
        assertEquals(900, musicExternalSystemEmptySpy.durationFreeTimeList.get(0));
        assertEquals(60, musicExternalSystemEmptySpy.durationFreeTimeList.get(1));
        assertEquals(250, musicExternalSystemEmptySpy.durationFreeTimeList.get(2));

        //Проверка хранилища
        assertEquals(2, storageSpy.schedules.size());
        Schedule schedule;
        //1
        schedule = storageSpy.schedules.get(0);
        assertEquals(56, schedule.getIdElem());
        assertEquals(Instant.parse("2020-05-10T12:00:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-05-10T14:00:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.PROGRAM, schedule.getType());
        assertEquals(7200, schedule.duration());
        //2
        schedule = storageSpy.schedules.get(1);
        assertEquals(6, schedule.getIdElem());
        assertEquals(Instant.parse("2020-05-10T15:00:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-05-10T16:50:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals( Type.PROGRAM, schedule.getType());
        assertEquals(6600, schedule.duration());
    }

    @Test
    void generateScheduleOnlyAdFrequencyZero()
    {
        UseCase useCase = new UseCase(storageSpy, blackBoxSpy, adExternalSystemSpy, musicExternalSystemEmptySpy, programExternalSystemSpy);
        Instant date = Instant.parse("2020-04-03T21:00:00Z"); //учитываем 3 часа
        useCase.generateSchedule(date);

        assertEquals("04:04:2020", programExternalSystemSpy.date);
        assertEquals("04:04:2020", adExternalSystemSpy.date);

        //Проверка МК
        assertEquals(1, musicExternalSystemEmptySpy.durationFreeTimeList.size());
        assertEquals(1440, musicExternalSystemEmptySpy.durationFreeTimeList.get(0));

        //Проверка хранилища
        assertEquals(0, storageSpy.schedules.size());
    }

    @Test
    void generateScheduleOnlyAdFrequencyOne()
    {
        MusicExternalSystemEmptySpy musicExternalSystemEmptySpy = new MusicExternalSystemEmptySpy();
        UseCase useCase = new UseCase(storageSpy, blackBoxSpy, adExternalSystemSpy, musicExternalSystemEmptySpy, programExternalSystemSpy);
        Instant date = Instant.parse("2020-04-04T21:00:00Z"); //учитываем 3 часа
        useCase.generateSchedule(date);

        assertEquals("05:04:2020", programExternalSystemSpy.date);
        assertEquals("05:04:2020", adExternalSystemSpy.date);

        //Проверка МК
        assertEquals(4, musicExternalSystemEmptySpy.durationFreeTimeList.size());
        assertEquals(358, musicExternalSystemEmptySpy.durationFreeTimeList.get(0));
        assertEquals(355, musicExternalSystemEmptySpy.durationFreeTimeList.get(1));
        assertEquals(357, musicExternalSystemEmptySpy.durationFreeTimeList.get(2));
        assertEquals(358, musicExternalSystemEmptySpy.durationFreeTimeList.get(3));

        //Проверка хранилища
        assertEquals(4, storageSpy.schedules.size());
        Schedule schedule;
        //1
        schedule = storageSpy.schedules.get(0);
        assertEquals(12, schedule.getIdElem());
        assertEquals(Instant.parse("2020-04-04T21:00:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-04-04T21:02:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.AD, schedule.getType());
        assertEquals(120, schedule.duration());
        //2
        schedule = storageSpy.schedules.get(1);
        assertEquals(112, schedule.getIdElem());
        assertEquals(Instant.parse("2020-04-05T03:00:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-04-05T03:05:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.AD, schedule.getType());
        assertEquals(300, schedule.duration());
        //3
        schedule = storageSpy.schedules.get(2);
        assertEquals(1, schedule.getIdElem());
        assertEquals(Instant.parse("2020-04-05T09:00:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-04-05T09:03:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.AD, schedule.getType());
        assertEquals(180, schedule.duration());
        //4
        schedule = storageSpy.schedules.get(3);
        assertEquals(88, schedule.getIdElem());
        assertEquals(Instant.parse("2020-04-05T15:00:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-04-05T15:02:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.AD, schedule.getType());
        assertEquals(120, schedule.duration());
    }

    @Test
    void generateScheduleOnlyAdFrequencyMoreOne()
    {
        MusicExternalSystemEmptySpy musicExternalSystemEmptySpy = new MusicExternalSystemEmptySpy();
        UseCase useCase = new UseCase(storageSpy, blackBoxSpy, adExternalSystemSpy, musicExternalSystemEmptySpy, programExternalSystemSpy);
        Instant date = Instant.parse("2020-04-05T21:00:00Z"); //учитываем 3 часа
        useCase.generateSchedule(date);

        assertEquals("06:04:2020", programExternalSystemSpy.date);
        assertEquals("06:04:2020", adExternalSystemSpy.date);

        //Проверка МК
        assertEquals(7, musicExternalSystemEmptySpy.durationFreeTimeList.size());
        assertEquals(203, musicExternalSystemEmptySpy.durationFreeTimeList.get(0));
        assertEquals(203, musicExternalSystemEmptySpy.durationFreeTimeList.get(1));
        assertEquals(200, musicExternalSystemEmptySpy.durationFreeTimeList.get(2));
        assertEquals(200, musicExternalSystemEmptySpy.durationFreeTimeList.get(3));
        assertEquals(202, musicExternalSystemEmptySpy.durationFreeTimeList.get(4));
        assertEquals(202, musicExternalSystemEmptySpy.durationFreeTimeList.get(5));
        assertEquals(207, musicExternalSystemEmptySpy.durationFreeTimeList.get(6));

        //Проверка хранилища
        assertEquals(7, storageSpy.schedules.size());
        Schedule schedule;
        //1
        schedule = storageSpy.schedules.get(0);
        assertEquals(22, schedule.getIdElem());
        assertEquals(Instant.parse("2020-04-05T21:00:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-04-05T21:02:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.AD, schedule.getType());
        assertEquals(120, schedule.duration());
        //2
        schedule = storageSpy.schedules.get(1);
        assertEquals(22, schedule.getIdElem());
        assertEquals(Instant.parse("2020-04-06T00:25:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-04-06T00:27:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.AD, schedule.getType());
        assertEquals(120, schedule.duration());
        //3
        schedule = storageSpy.schedules.get(2);
        assertEquals(342, schedule.getIdElem());
        assertEquals(Instant.parse("2020-04-06T03:50:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-04-06T03:55:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.AD, schedule.getType());
        assertEquals(300, schedule.duration());
        //4
        schedule = storageSpy.schedules.get(3);
        assertEquals(342, schedule.getIdElem());
        assertEquals(Instant.parse("2020-04-06T07:15:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-04-06T07:20:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.AD, schedule.getType());
        assertEquals(300, schedule.duration());
        //5
        schedule = storageSpy.schedules.get(4);
        assertEquals(5, schedule.getIdElem());
        assertEquals(Instant.parse("2020-04-06T10:40:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-04-06T10:43:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.AD, schedule.getType());
        assertEquals(180, schedule.duration());
        //6
        schedule = storageSpy.schedules.get(5);
        assertEquals(5, schedule.getIdElem());
        assertEquals(Instant.parse("2020-04-06T14:05:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-04-06T14:08:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.AD, schedule.getType());
        assertEquals(180, schedule.duration());
        //7
        schedule = storageSpy.schedules.get(6);
        assertEquals(5, schedule.getIdElem());
        assertEquals(Instant.parse("2020-04-06T17:30:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-04-06T17:33:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.AD, schedule.getType());
        assertEquals(180, schedule.duration());
    }

    @Test
    void generateScheduleEmptyOnlyMusic()
    {
        Instant date = Instant.parse("2020-05-17T21:00:00Z"); //учитываем 3 часа
        useCase.generateSchedule(date);

        assertEquals("18:05:2020", programExternalSystemSpy.date);
        assertEquals("18:05:2020", adExternalSystemSpy.date);

        //Проверка МК
        assertEquals(1, musicExternalSystemSpy.durationFreeTimeList.size());
        assertEquals(1440, musicExternalSystemSpy.durationFreeTimeList.get(0));

        //Проверка хранилища
        assertEquals(5, storageSpy.schedules.size());
        Schedule schedule;
        //1
        schedule = storageSpy.schedules.get(0);
        assertEquals(4, schedule.getIdElem());
        assertEquals(Instant.parse("2020-05-17T21:00:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-05-17T21:03:04Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.MUSIC, schedule.getType());
        assertEquals(184, schedule.duration());
        assertEquals("q-1", schedule.getHash());
        //2
        schedule = storageSpy.schedules.get(1);
        assertEquals(6, schedule.getIdElem());
        assertEquals(Instant.parse("2020-05-17T21:03:04Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-05-17T21:05:18Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.MUSIC, schedule.getType());
        assertEquals(134, schedule.duration());
        assertEquals("q-2", schedule.getHash());
        //3
        schedule = storageSpy.schedules.get(2);
        assertEquals(9, schedule.getIdElem());
        assertEquals(Instant.parse("2020-05-17T21:05:18Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-05-17T21:08:14Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.MUSIC, schedule.getType());
        assertEquals(176, schedule.duration());
        assertEquals("q-4", schedule.getHash());
        //4
        schedule = storageSpy.schedules.get(3);
        assertEquals(13, schedule.getIdElem());
        assertEquals(Instant.parse("2020-05-17T21:08:14Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-05-17T21:12:24Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.MUSIC, schedule.getType());
        assertEquals(250, schedule.duration());
        assertEquals("q-5", schedule.getHash());
        //5
        schedule = storageSpy.schedules.get(4);
        assertEquals(17, schedule.getIdElem());
        assertEquals(Instant.parse("2020-05-17T21:12:24Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-05-17T21:14:49Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.MUSIC, schedule.getType());
        assertEquals(145, schedule.duration());
        assertEquals("q-3", schedule.getHash());
    }

    @Test
    void generateSchedule()
    {
        Instant date = Instant.parse("2019-12-31T21:00:00Z"); //учитываем 3 часа
        useCase.generateSchedule(date);

        assertEquals("01:01:2020", programExternalSystemSpy.date);
        assertEquals("01:01:2020", adExternalSystemSpy.date);

        //Проверка МК
        assertEquals(4, musicExternalSystemSpy.durationFreeTimeList.size());
        assertEquals(176, musicExternalSystemSpy.durationFreeTimeList.get(0));
        assertEquals(210, musicExternalSystemSpy.durationFreeTimeList.get(1));
        assertEquals(420, musicExternalSystemSpy.durationFreeTimeList.get(2));
        assertEquals(475, musicExternalSystemSpy.durationFreeTimeList.get(3));

        //Проверка хранилища
        assertEquals(11, storageSpy.schedules.size());
        Schedule schedule;
        //1
        schedule = storageSpy.schedules.get(0);
        assertEquals(3, schedule.getIdElem());
        assertEquals(Instant.parse("2020-01-01T00:00:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-01-01T00:30:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.PROGRAM, schedule.getType());
        assertEquals(1800, schedule.duration());
        //2
        schedule = storageSpy.schedules.get(1);
        assertEquals(9, schedule.getIdElem());
        assertEquals(Instant.parse("2020-01-01T04:00:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-01-01T06:00:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals( Type.PROGRAM, schedule.getType());
        assertEquals(7200, schedule.duration());
        //3
        schedule = storageSpy.schedules.get(2);
        assertEquals(25, schedule.getIdElem());
        assertEquals(Instant.parse("2019-12-31T21:00:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2019-12-31T21:04:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals( Type.AD, schedule.getType());
        assertEquals(240, schedule.duration());
        //4
        schedule = storageSpy.schedules.get(3);
        assertEquals(250, schedule.getIdElem());
        assertEquals(Instant.parse("2020-01-01T05:00:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-01-01T05:05:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals( Type.AD, schedule.getType());
        assertEquals(300, schedule.duration());
        //5
        schedule = storageSpy.schedules.get(4);
        assertEquals(250, schedule.getIdElem());
        assertEquals(Instant.parse("2020-01-01T13:00:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-01-01T13:05:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals( Type.AD, schedule.getType());
        assertEquals(300, schedule.duration());
        //6
        schedule = storageSpy.schedules.get(5);
        assertEquals(41, schedule.getIdElem());
        assertEquals(Instant.parse("2019-12-31T21:04:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2019-12-31T21:07:04Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.MUSIC, schedule.getType());
        assertEquals(184, schedule.duration());
        assertEquals("w-1", schedule.getHash());
        //7
        schedule = storageSpy.schedules.get(6);
        assertEquals(61, schedule.getIdElem());
        assertEquals(Instant.parse("2019-12-31T21:07:04Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2019-12-31T21:09:18Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.MUSIC, schedule.getType());
        assertEquals(134, schedule.duration());
        assertEquals("w-2", schedule.getHash());
        //8
        schedule = storageSpy.schedules.get(7);
        assertEquals(19, schedule.getIdElem());
        assertEquals(Instant.parse("2019-12-31T21:09:18Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2019-12-31T21:12:14Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.MUSIC, schedule.getType());
        assertEquals(176, schedule.duration());
        assertEquals("w-4", schedule.getHash());
        //9
        schedule = storageSpy.schedules.get(8);
        assertEquals(24, schedule.getIdElem());
        assertEquals(Instant.parse("2020-01-01T06:00:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-01-01T06:04:10Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.MUSIC, schedule.getType());
        assertEquals(250, schedule.duration());
        assertEquals("u-1", schedule.getHash());
        //10
        schedule = storageSpy.schedules.get(9);
        assertEquals(26, schedule.getIdElem());
        assertEquals(Instant.parse("2020-01-01T06:04:10Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-01-01T06:09:10Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.MUSIC, schedule.getType());
        assertEquals(300, schedule.duration());
        assertEquals("u-2", schedule.getHash());
        //11
        schedule = storageSpy.schedules.get(10);
        assertEquals(117, schedule.getIdElem());
        assertEquals(Instant.parse("2020-01-01T13:05:00Z").getEpochSecond(), schedule.getStartDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Instant.parse("2020-01-01T13:15:00Z").getEpochSecond(), schedule.getEndDate().getEpochSecond()); //учитываем 3 часа
        assertEquals(Type.MUSIC, schedule.getType());
        assertEquals(600, schedule.duration());
        assertEquals("a-3", schedule.getHash());
    }

    @Test
    void viewScheduleEmpty()
    {
        Instant date = Instant.parse("2020-01-31T21:00:00Z"); //учитываем 3 часа
        List<ScheduleSimple> scheduleSimples = useCase.viewSchedule(date);
        assertEquals(0, scheduleSimples.size());
        assertEquals(0, storageSpy.schedules.size());
        assertEquals("01:02:2020", storageSpy.date);
    }

    @Test
    void viewSchedule()
    {
        Instant date = Instant.parse("2020-02-01T21:00:00Z"); //учитываем 3 часа
        List<ScheduleSimple> scheduleSimples = useCase.viewSchedule(date);

        assertEquals(4, scheduleSimples.size());
        assertEquals(4, storageSpy.schedules.size());
        assertEquals("02:02:2020", storageSpy.date);

        ScheduleSimple schedule;
        //1
        schedule = scheduleSimples.get(0);
        assertEquals("01:00", schedule.getStartTime());
        assertEquals("01:07", schedule.getEndTime());
        assertEquals("Музыка", schedule.getType());
        //2
        schedule = scheduleSimples.get(1);
        assertEquals("09:00", schedule.getStartTime());
        assertEquals("11:00", schedule.getEndTime());
        assertEquals("Программа", schedule.getType());
        //3
        schedule = scheduleSimples.get(2);
        assertEquals("18:00", schedule.getStartTime());
        assertEquals("18:10", schedule.getEndTime());
        assertEquals("Реклама", schedule.getType());
        //4
        schedule = scheduleSimples.get(3);
        assertEquals("20:00", schedule.getStartTime());
        assertEquals("20:05", schedule.getEndTime());
        assertEquals("Реклама", schedule.getType());
    }

    @Test
    void getIdProgramOnline()
    {
        Instant date = Instant.parse("2007-12-13T07:15:00Z"); //учитываем 3 часа
        String expectedDate = "to_timestamp('13-12-2007 10:15', 'dd.mm.yyyy hh24:mi')";
        int idExpected = 14;
        int idActual = useCase.startLiveBroadcast(date);
        assertEquals(expectedDate, storageSpy.date);
        assertEquals(idExpected, idActual);
        assertTrue(blackBoxSpy.flagOnline);
    }

    @Test
    void getIdNoProgramOnline()
    {
        Instant date = Instant.parse("2008-12-13T07:15:00Z"); //учитываем 3 часа
        String expectedDate = "to_timestamp('13-12-2008 10:15', 'dd.mm.yyyy hh24:mi')";
        int idExpected = -1;
        int idActual = useCase.startLiveBroadcast(date);
        assertEquals(expectedDate, storageSpy.date);
        assertEquals(idExpected, idActual);
        assertFalse(blackBoxSpy.flagOnline);
    }

    @Test
    void endLiveBroadcast()
    {
        int id = 66;
        String hash = "qwerty";
        useCase.endLiveBroadcast(id);
        assertEquals(String.valueOf(id), storageSpy.id);
        assertEquals(hash, storageSpy.hash);
        assertFalse(blackBoxSpy.flagOnline);
    }

    @Test
    void viewMusicListForPlayOnline()
    {
        List<Music> musics = useCase.viewMusicListForPlayOnline();
        assertEquals(2, musics.size());
        Music m;
        //1
        m = musics.get(0);
        assertEquals(2, m.getId());
        assertEquals(301, m.getDuration());
        assertEquals("p-1", m.getHash());
        assertEquals("Smells Like Teen Spirit", m.getNameMusic());
        assertEquals("Nirvana", m.getNameArtist());
        //2
        m = musics.get(1);
        assertEquals(22, m.getId());
        assertEquals(121, m.getDuration());
        assertEquals("p-2", m.getHash());
        assertEquals("We Will Rock You", m.getNameMusic());
        assertEquals("Queen", m.getNameArtist());
    }
}