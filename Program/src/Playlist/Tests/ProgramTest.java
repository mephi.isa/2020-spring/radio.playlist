package Playlist.Tests;

import Playlist.Dependencies.Struct.Program;
import Playlist.Domain.Schedule;
import Playlist.Enums.Type;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class ProgramTest {

    @Test
    void programToSchedule()
    {
        int idElemExpected = 88;
        int durationExpected = 47;
        int idExpected = 0;
        Instant startDateExpected = Instant.parse("2020-05-10T06:59:00Z");
        Program program = new Program(idElemExpected, durationExpected, startDateExpected);
        Schedule schedule = program.programToSchedule(idExpected);
        assertEquals(idExpected, schedule.getId());
        assertEquals(idElemExpected, schedule.getIdElem());
        assertEquals(durationExpected, schedule.duration());
        assertTrue(startDateExpected.equals(schedule.getStartDate()));
        assertNull(schedule.getHash());
        assertEquals(Type.PROGRAM, schedule.getType());
    }
}