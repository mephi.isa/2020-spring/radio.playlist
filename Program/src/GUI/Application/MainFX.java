package GUI.Application;

import GUI.Application.AppFX;
import Playlist.Application.UseCase;
import Playlist.Dependencies.Struct.ScheduleSimple;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static GUI.Application.AppFX.useCase;

public class MainFX
{
    @FXML
    private DatePicker dpViewDate;

    @FXML
    private DatePicker dpGenDate;

    @FXML
    private AnchorPane apScene;

    @FXML
    private Button bViewSchedule;

    @FXML
    private Button bGenerate;

    @FXML
    private Button bPlayOnline;

    @FXML
    private ToggleButton tbChangeLive;

    private int idProgramOnline;

    @FXML
    private void viewSchedule() throws IOException
    {
        LocalDate date = dpViewDate.getValue();
        if (date==null)
        {
            dpViewDate.setStyle("-fx-background-color: red");
        }
        else
        {
            AppFX.loadSceneViewSchedule(date);
        }
    }

    @FXML
    private void generateSchedule() throws IOException
    {
        LocalDate date = dpGenDate.getValue();
        if (date==null)
        {
            dpGenDate.setStyle("-fx-background-color: red");
        }
        else
        {
            Instant instant = date.atStartOfDay(UseCase.zone).toInstant();
            List<ScheduleSimple> scheduleSimple = useCase.viewSchedule(Instant.from(instant));
            if (scheduleSimple.size()==0 && instant.getEpochSecond()>Instant.now().getEpochSecond())
            {
                useCase.generateSchedule(instant);
                AppFX.loadSceneViewSchedule(date);
            }
            else
            {
                dpGenDate.setStyle("-fx-background-color: red");
            }
        }
    }

    @FXML
    private void playOnline() throws IOException
    {
        AppFX.loadScenePlayOnline();
    }

    @FXML
    private void startLiveBroadcast()
    {
        if(tbChangeLive.isSelected())
        {
            idProgramOnline = useCase.startLiveBroadcast(Instant.now());
            if (idProgramOnline==-1)
            {
                tbChangeLive.setSelected(false);
                tbChangeLive.setStyle("-fx-border-color: red; -fx-border-width: 2px;");
            }
            else
            {
                setDisableButton(true);
                dpGenDate.setStyle("-fx-background-color: grey");
                dpViewDate.setStyle("-fx-background-color: grey");
                tbChangeLive.setStyle("-fx-border-color: ; -fx-border-width: ;");
                tbChangeLive.setStyle("-fx-background-color: green");
                tbChangeLive.setText("End Live Broadcast");
            }
        }
        else
        {
            setDisableButton(false);
            tbChangeLive.setStyle("");
            tbChangeLive.setText("Start Live Broadcast");
            useCase.endLiveBroadcast(idProgramOnline);
            idProgramOnline = -1;
        }
    }

    public void initialize(double width, double height)
    {
        apScene.setPrefSize(width, height);
        javafx.scene.image.Image image = new Image(AppFX.urlBackground, width, height,false,true);
        BackgroundImage background = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        BackgroundFill backgroundFill = new BackgroundFill(Color.SKYBLUE, CornerRadii.EMPTY, Insets.EMPTY);
        apScene.setBackground(new Background(Collections.singletonList(backgroundFill), Collections.singletonList(background)));
    }

    private void setDisableButton(boolean flag)
    {
        bViewSchedule.setDisable(flag);
        bGenerate.setDisable(flag);
        bPlayOnline.setDisable(flag);
        dpViewDate.setDisable(flag);
        dpGenDate.setDisable(flag);
    }
}
