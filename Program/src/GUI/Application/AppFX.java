package GUI.Application;

import Playlist.Application.UseCase;
import Playlist.DB.StorageH2;
import Playlist.Dependencies.Interface.*;
import Playlist.Dependencies.Struct.Ad;
import Playlist.Dependencies.Struct.Music;
import Playlist.Dependencies.Struct.Program;
import Playlist.Domain.Schedule;
import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.w3c.dom.ls.LSOutput;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static Playlist.Dependencies.DateConvert.setZeroDateFormat;
import static Playlist.Tests.GenerateRandomString.generateRandomStringUsingSecureRandom;

public class AppFX extends Application
{
    private static final double width = 600;
    private static final double height = 400;
    private static String urlSceneViewSchedule = "ViewScheduleFX.fxml";
    private static String urlSceneMain = "MainFX.fxml";
    private static String urlScenePlayOnline = "PlayOnlineFX.fxml";
    public static String urlBackground = "GUI/Resources/background.jpg";
    private static Stage primaryStage;

    private static MusicExternalSystemSpy musicExternalSystem;
    private static AdExternalSystemSpy adExternalSystem;
    private static ProgramExternalSystemSpy programExternalSystem;
    private static StorageH2 storage;
    private static BlackBoxSpy blackBox;
    public static UseCase useCase;

    private static class ProgramExternalSystemSpy implements ProgramExternalSystem
    {
        public List<Program> getPrograms(String date)
        {
            List<Program> programs = new ArrayList<Program>(3);
            Random random = new Random();
            String dateConvert = String.valueOf(date.charAt(6))+String.valueOf(date.charAt(7))+String.valueOf(date.charAt(8))+String.valueOf(date.charAt(9))+"-"+String.valueOf(date.charAt(3))+String.valueOf(date.charAt(4))+"-"+String.valueOf(date.charAt(0))+String.valueOf(date.charAt(1))+"T";
            int n = random.nextInt(3);
            for (int i=0; i<n; i++)
            {
                int duration = (random.nextInt(115)+15)*60;
                String charStart = setZeroDateFormat(String.valueOf(random.nextInt(6)+i*8));
                Instant startDate = Instant.parse(dateConvert+charStart+":00:00Z");
                programs.add(new Program(random.nextInt(10000), duration, Instant.ofEpochSecond(startDate.getEpochSecond()-3*60*60))); //учтем 3 часа
            }
            return programs;
        }
    }

    private static class MusicExternalSystemSpy implements MusicExternalSystem
    {
        public List<Music> getMusics(int durationFreeTime)
        {
            List <Music> musics = new ArrayList<Music>();
            Random random = new Random();
            int duration = random.nextInt(600)+60;
            int currentDuration = duration;
            while (currentDuration/60<=durationFreeTime)
            {
                musics.add(new Music(random.nextInt(10000)+20, duration, generateRandomStringUsingSecureRandom()));
                duration = random.nextInt(600)+60;
                currentDuration+=duration;
            }
            return musics;
        }

        public List<Music> getMusics()
        {
            List <Music> musics = new ArrayList<Music>(5);
            musics.add(new Music(1, 301, "p-1", "Smells Like Teen Spirit", "Nirvana"));
            musics.add(new Music(2, 121, "p-2", "We Will Rock You", "Queen"));
            musics.add(new Music(3, 264, "p-3", "Sunday Bloody Sunday", "U2"));
            musics.add(new Music(4, 259, "p-4", "I Was Made For Lovin' You", "Kiss"));
            musics.add(new Music(5, 251, "p-5", "Wonderwall", "Oasis"));
            return musics;
        }
    }

    private static class StorageSpy implements Storage
    {

        public void setListSchedule(List<Schedule> schedules)
        {
        }

        public void setSchedule(Schedule schedule)
        {
        }

        public void updateHashStorage(String id, String hash)
        {
        }

        public List<Schedule> getSchedules(String date)
        {
            return null;
        }

        public String getHash(String id)
        {
            return null;
        }

        public int getId(String date)
        {
            return -1;
        }
    }

    private static class AdExternalSystemSpy implements AdExternalSystem
    {
        public List<Ad> getAds(String date)
        {
            List<Ad> ads = new ArrayList<Ad>(30);
            Random random = new Random();
            int n = random.nextInt(10);
            for (int i=0; i<n; i++)
                ads.add(new Ad(random.nextInt(10000), random.nextInt(3)+1,random.nextInt(600)+60));
            return ads;
        }

        public Ad getAd()
        {
            Random random = new Random();
            return new Ad(random.nextInt(10000), random.nextInt(600)+60, generateRandomStringUsingSecureRandom());
        }

        public String getHash(int id)
        {
            return generateRandomStringUsingSecureRandom();
        }
    }

    private static class BlackBoxSpy implements BlackBox
    {
        public void playFile(String hash)
        {
        }

        public void startOnline()
        {
        }

        public String endOnline()
        {
            return generateRandomStringUsingSecureRandom();
        }
    }

    public static void main(String[] args)
    {
        musicExternalSystem = new MusicExternalSystemSpy();
        adExternalSystem = new AdExternalSystemSpy();
        programExternalSystem = new ProgramExternalSystemSpy();
        storage = new StorageH2();
        blackBox = new BlackBoxSpy();
        useCase = new UseCase(storage, blackBox, adExternalSystem, musicExternalSystem, programExternalSystem);
        Application.launch();
    }

    @Override
    public void start(Stage stage) throws Exception
    {
        primaryStage = stage;
        primaryStage.setTitle("Radio");
        primaryStage.getIcons().add(new Image("GUI/Resources/mephi_logo.png"));
        primaryStage.setMinWidth(width);
        primaryStage.setMinHeight(height);
        primaryStage.setMaxWidth(width+50);
        primaryStage.setMaxHeight(height+50);
        loadSceneMain();
    }

    public static void loadSceneViewSchedule(LocalDate date) throws IOException
    {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(AppFX.class.getResource(urlSceneViewSchedule));
        Parent root = loader.load();

        ViewScheduleFX load = loader.<ViewScheduleFX>getController();
        load.initialize(width, height, date);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void loadSceneMain() throws IOException
    {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(AppFX.class.getResource(urlSceneMain));
        Parent root = loader.load();

        MainFX load = loader.<MainFX>getController();
        load.initialize(width, height);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void loadScenePlayOnline() throws IOException
    {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(AppFX.class.getResource(urlScenePlayOnline));
        Parent root = loader.load();

        PlayOnlineFX load = loader.<PlayOnlineFX>getController();
        load.initialize(width, height);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
