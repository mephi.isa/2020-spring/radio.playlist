package GUI.Application;

import Playlist.Application.UseCase;
import Playlist.Dependencies.Struct.ScheduleSimple;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Collections;

import static GUI.Application.AppFX.useCase;

public class ViewScheduleFX
{
    @FXML
    private Button bRefresh;

    @FXML
    private Button bBack;

    @FXML
    private DatePicker dpDate;

    @FXML
    private AnchorPane apScene;

    @FXML
    private TableView<ScheduleSimple> tvSchedule;

    @FXML
    private TableColumn<ScheduleSimple, String> tcType;

    @FXML
    private TableColumn<ScheduleSimple, String> tcStartTime;

    @FXML
    private TableColumn<ScheduleSimple, String> tcEndTime;

    private ObservableList<ScheduleSimple> scheduleList;

    private LocalDate date;

    @FXML
    private void refreshSchedule()
    {
        LocalDate date = dpDate.getValue();
        if (date==null)
        {
            dpDate.setStyle("-fx-background-color: red");
        }
        else
        {
            this.date = date;
            dpDate.setStyle("-fx-background-color: grey");
            scheduleList.clear();
            setTable();
        }
    }

    @FXML
    private void backMain() throws IOException
    {
        AppFX.loadSceneMain();
    }

    public void initialize(double width, double height, LocalDate date)
    {
        apScene.setPrefSize(width, height);
        javafx.scene.image.Image image = new Image(AppFX.urlBackground, width, height,false,true);
        BackgroundImage background = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        BackgroundFill backgroundFill = new BackgroundFill(Color.SKYBLUE, CornerRadii.EMPTY, Insets.EMPTY);
        apScene.setBackground(new Background(Collections.singletonList(backgroundFill), Collections.singletonList(background)));
        tcType.setCellValueFactory(new PropertyValueFactory<>("type"));
        tcStartTime.setCellValueFactory(new PropertyValueFactory<>("startTime"));
        tcEndTime.setCellValueFactory(new PropertyValueFactory<>("endTime"));
        this.date = date;
        dpDate.setValue(date);

        setTable();
    }

    private void setTable()
    {
        Instant instant = date.atStartOfDay(UseCase.zone).toInstant();
        scheduleList = FXCollections.observableArrayList(useCase.viewSchedule(instant));
        tvSchedule.setItems(scheduleList);
    }
}
