package GUI.Application;

import Playlist.Dependencies.Struct.Ad;
import Playlist.Dependencies.Struct.Music;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.time.Instant;
import java.util.Collections;

import static GUI.Application.AppFX.useCase;

public class PlayOnlineFX
{
    @FXML
    private AnchorPane apScene;

    @FXML
    private Button bPlayAd;

    @FXML
    private Button bPlayMusic;

    @FXML
    private TableView<Music> tvMusic;

    @FXML
    private TableColumn<Music, Integer> tcMusicId;

    @FXML
    private TableColumn<Music, Long> tcMusicDuration;

    @FXML
    private TableColumn<Music, String> tcNameArtist;

    @FXML
    private TableColumn<Music, String> tcNameMusic;

    @FXML
    private Button bBack;

    private ObservableList<Music> musicList;

    @FXML
    private void backMain() throws IOException
    {
        AppFX.loadSceneMain();
    }

    @FXML
    private void playAd()
    {
        useCase.playAdOnline();
    }

    @FXML
    private void playMusic()
    {
        int rowIndex = tvMusic.getSelectionModel().getSelectedIndex();
        if (rowIndex>=0)
        {
            Music music = musicList.get(rowIndex);
            AppFX.useCase.playMusicOnline(music);
        }
    }

    public void initialize(double width, double height)
    {
        apScene.setPrefSize(width, height);
        javafx.scene.image.Image image = new Image(AppFX.urlBackground, width, height,false,true);
        BackgroundImage background = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        BackgroundFill backgroundFill = new BackgroundFill(Color.SKYBLUE, CornerRadii.EMPTY, Insets.EMPTY);
        apScene.setBackground(new Background(Collections.singletonList(backgroundFill), Collections.singletonList(background)));

        tcMusicId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tcMusicDuration.setCellValueFactory(new PropertyValueFactory<>("duration"));
        tcNameArtist.setCellValueFactory(new PropertyValueFactory<>("nameArtist"));
        tcNameMusic.setCellValueFactory(new PropertyValueFactory<>("nameMusic"));
        musicList = FXCollections.observableArrayList(useCase.viewMusicListForPlayOnline());
        tvMusic.setItems(musicList);
    }
}
